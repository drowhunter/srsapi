using System;
using System.Net.Sockets;
using System.Runtime.InteropServices;
using System.Threading;

namespace csharp_api
{

    [StructLayout(LayoutKind.Sequential)]
    public struct telemetryPacket
    {
        [MarshalAs(UnmanagedType.ByValArray, SizeConst = 3)]
        public char[] api_mode;
        public uint version;
        public float leftFanPower;
        public float rightFanPower;
        public float rpm;
        public float maxRpm;
        public int gear;

        public telemetryPacket(char[] pmode, uint pversion, float pleftFanPower, float prightFanPower, float prpm, float pmaxRpm, int pgear)
        {
            api_mode = pmode;
            version = pversion;
            leftFanPower = pleftFanPower;
            rightFanPower = prightFanPower;
            rpm = prpm;
            maxRpm = pmaxRpm;
            gear = pgear;
        }
    }


    class Program
    {

        static void Main(string[] args)
        {

            string SRS_COMPUTER_IP = "localhost";  //to broadcast to all computers in the network use ""
            int SRS_COMPUTER_PORT = 33001;  //this port can be changed on the config.ini of srs app
            char[] PACKET_HEADER = "lla".ToCharArray();  //constant to identify the package
            uint API_VERSION = 101;  //constant of the current version of the api

            UdpClient udpClient = new UdpClient();

            // define variables
            float maxRpm = 8000;
            double speed = 0;
            float rpm = 0;
            int gear = 0;


            // define increments
            double speedIncrement = 0.2;

            while (true)
            {
                // when speed hits 200 starts decreasing the speed
                if (speed > 100)
                {
                    speed = 100;
                    speedIncrement = -0.2;
                }

                // when speed hits 0, starts acceleration again
                if (speed < 0)
                {
                    speed = 0;
                    speedIncrement = 0.2;
                }


                // reset the rpm
                if (rpm > maxRpm) rpm = 0;

                // reset gear to 0
                if (gear > 9) gear = 0;

                telemetryPacket tp = new telemetryPacket(PACKET_HEADER, API_VERSION, (float)speed, (float)(100 - speed), rpm, maxRpm, gear);


                int size = Marshal.SizeOf(tp);
                byte[] packet = new byte[size];

                IntPtr ptr = Marshal.AllocHGlobal(size);
                Marshal.StructureToPtr(tp, ptr, true);
                Marshal.Copy(ptr, packet, 0, size);

                udpClient.Send(packet, packet.Length, SRS_COMPUTER_IP, SRS_COMPUTER_PORT);


                System.Console.WriteLine("sending... " + string.Join("", tp.api_mode) + " " + tp.version + " " + tp.rpm + " " + tp.leftFanPower + " " + tp.rightFanPower);

                // increment variables
                gear = gear + 1;
                rpm = rpm + 50;
                speed = speed + speedIncrement;

                Thread.Sleep(30);

            }
        }
    }
}
